package com.zuitt.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}

	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	@PostMapping("/users")
	public String createUser(){
		return "New user created" ;
	}

	@GetMapping("/users/{userId}")
	public String getUserByUserId(@PathVariable Long userId){
		return "The user with the ID " + userId + " is being retrieved.";
	}

	@DeleteMapping("/users/{userId}")
	public String deleteUser(@PathVariable Long userId, @RequestHeader(value = "Authorization", required = false) String authorizationHeader){
		if (authorizationHeader == null || authorizationHeader.isEmpty()) {
			return "Unauthorized access";
		}
		return "The user " + userId + " has been deleted";
	}

	@PutMapping("/users/{userId}")
	@ResponseBody
	public User updateUser(@PathVariable Long userId, @RequestBody User user){
		return user;
	}
}

