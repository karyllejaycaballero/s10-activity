package com.zuitt.activity;

public class User {
    private String name;

    public User(String name, int age) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

